/*
 * Author: Immanuel Rajkumar Philip Karunakaran
 * Visiting permutations/combinations of distinct elements with precedence conditions
 * Reference: Donald Knuth's Art of Programming
 */

#include<iostream>
#include<ctime>
#include<fstream>
#include<cmath>

using namespace std;

int count; // variable to store number of permutations/combinations
int n; // input array length
int c; // precedence set count
int v; // variable for storing verbose option

/*
Visit function - invoked for each permutations/combinations
Function displays the permutation/combination if verbose parameter is greater than 0
*/
int visit( int A[], int verbose )
{
    if( verbose > 0 ) {
        for( int i=0; i<n; i++ )
            cout<<A[i]<<" ";
        cout<<endl;
    }
    count++;
    return 0;
}

/*Function that takes the array where permutation is generated as input, the precedence matrix (cX2),
 * the element to be inserted i and display mode, verbose.
 * Repeatedly checks if element i can be inserted at a position satisfying the precedence conditions
 * */
void permute( int *A, int **TP, int i, int verbose )
{
    if( i == 0 ) {
        visit(A, verbose);
    } else {
        for( int k=0; k<n; k++ ) {
            bool bflag = true; // break flag, bflag, will become false if the precedence condition is violated
            if(A[k] == 0 ) { //if the position is vacant
                for(int y=0; y<c && bflag; y++ ) { // check each precedence matrix condition
                    // See if all the elements already present in the array before the insertion point do not violate precedence.
                    for(int b=0; b<k; b++ ) {
                        if( i==TP[y][0] && A[b]==TP[y][1]) {
                            bflag = false;
                            break;
                        }
                    }
                    // See if all the elements already present in the array after the insertion point do not violate precedence.
                    for(int b=k+1; b<n && bflag; b++) {
                        if( i==TP[y][1] && A[b]==TP[y][0]) {
                            bflag = false;
                            break;
                        }
                    }

                }
                //insert the element i, if the precedence conditions are not violated
                if( bflag ) {
                    A[k] = i;
                    permute(A, TP, i-1, verbose);
                    A[k] = 0;
                }
            }
        }
    }
}

int main()
{
    clock_t start, end;
    long long dur = 0;

    n=0;
    count=0;
    v=0;

    cin>>n>>c>>v;
    // Start timer after getting the input
    start = clock();

    //Precedence matrix, cX2
    int ** TP = new int*[c];
    for(int i=0; i<c; i++ )
        TP[i] = new int[2];

    for(int i=0; i<c; i++)
        for(int j=0; j<2; j++)
            cin>>TP[i][j];

    //Array where permutation happens
    int * A = new int(n);

    for(int i=0; i<n; i++)
        A[i] = 0;

    permute(A, TP, n, v);

    end = clock();

    dur = (long long) round((long long)(end - start)/CLOCKS_PER_SEC*1000);
    cout<<count<<" "<<dur<<endl;

    return 0;
}
